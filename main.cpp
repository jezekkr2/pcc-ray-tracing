#include <iostream>
#include <vector>
#include <thread>
#include <fstream>
#include <sstream>
#include <iomanip>

#include "ray.h"
#include "hitable_list.h"
#include "sphere.h"
#include "camera.h"
#include "material.h"
#include "timer.h"
#include "texture.h"
#include "rectangle.h"
#include "box.h"
#include "bvh_node.h"

void output_ppm(int width, int height, std::vector<std::vector<vec3>>&  pixel_data, const std::string file_name) {
    std::ofstream out_file(file_name);

    out_file << "P3" << std::endl;
    out_file << width << " " << height << std::endl;
    out_file << "255" << std::endl;

    std::cout << "Generating file " + file_name + " in PPM format...\n";

    for (int y = height-1; y >= 0; y--) {
        for (int x = 0; x < width; x++) {
            vec3 col = pixel_data[y][x];
            int ir = int(255.99 * col[0]);
            int ig = int(255.99 * col[1]);
            int ib = int(255.99 * col[2]);
            out_file << ir << " " << ig << " " << ib << " ";
        }
        out_file << std::endl;
    }

    out_file.close();

    std::cout << "Finished, you can find the result in executable's folder.\n";

}

vec3 color(const ray& r, const vec3& bcg, hitable& world, int depth) {


    hit_record record;
    vec3 finalColor = vec3(0,0,0);

    if (depth <= 0)
        return finalColor;

    if(!world.hit(r,0.001, infinity, record)){
        finalColor = bcg;
    }
    else {
        vec3 emitted = record.mat_ptr->emitted(record.u, record.v, record.p);
        finalColor += emitted;

        ray scatteredRay;
        vec3 attenuation;

        if(record.mat_ptr->scatter(r,record, attenuation,scatteredRay)){
            finalColor += attenuation * color(scatteredRay, bcg, world, depth-1);
        }
    }
    return finalColor;
}

vec3 hextorgb(char const *hex) {
    int r,g,b;
    std::sscanf(hex, "#%02x%02x%02x", &r, &g, &b);

    vec3 rgb = vec3(r / 255.0,g / 255.0,b / 255.0);
    return rgb;

}

hitable_list sphere_mats(){
    auto checker = make_shared<checker_texture>(vec3(hextorgb("#ffffff")), vec3(hextorgb("#000000")));
    std::cout << "Generating sphere_mats scene...\n";
    hitable_list world;
    world.add(make_shared<sphere>(vec3(0,0,0), 0.5, make_shared<lambertian>(checker)));
    world.add(make_shared<sphere>(vec3(0,-100.5,0), 100, make_shared<lambertian>(checker)));
    world.add(make_shared<sphere>(vec3(0.65,0,-.45), 0.5, make_shared<metal>(hextorgb("#ff8c75"), 0.2)));
    world.add(make_shared<sphere>(vec3(-0.65,0,-.45), 0.5, make_shared<dielectric>(1.55)));
    return world;
}

hitable_list cornell_box(){

    hitable_list world;

    auto red   = make_shared<lambertian>(vec3(.65, .05, .05));
    auto white = make_shared<lambertian>(vec3(.73, .73, .73));
    auto green = make_shared<lambertian>(vec3(.12, .45, .15));
    auto light = make_shared<diffuse_light>(vec3(15, 15, 15));

    std::cout << "Generating cornell_box scene...\n";

    world.add(make_shared<yz_rect>(0, 555, 0, 555, 555, green));
    world.add(make_shared<yz_rect>(0, 555, 0, 555, 0, red));
    world.add(make_shared<xz_rect>(213, 343, 227, 332, 554, light));
    world.add(make_shared<xz_rect>(0, 555, 0, 555, 555, white));
    world.add(make_shared<xz_rect>(0, 555, 0, 555, 0, white));
    world.add(make_shared<xy_rect>(0, 555, 0, 555, 555, white));

    shared_ptr<hitable> box1 = make_shared<boxobject>(vec3(0, 0, 0), vec3(165, 330, 165), white);
    box1 = make_shared<rotate_y>(box1, 15);
    box1 = make_shared<translate>(box1, vec3(265,0,295));
    world.add(box1);

    shared_ptr<hitable> box2 = make_shared<boxobject>(vec3(0,0,0), vec3(165,165,165), white);
    box2 = make_shared<rotate_y>(box2, -18);
    box2 = make_shared<translate>(box2, vec3(130,0,65));
    world.add(box2);

    return world;

}

hitable_list menger_sponge(int level, vec3 cube_size, vec3 corner) {
    hitable_list sponge;

    auto white = make_shared<lambertian>(hextorgb("#ff6826"));



    if (level <= 0) {
        //std::cout << counter++ << "\n";
        auto cube = make_shared<boxobject>(corner, corner + cube_size, white);
        //std::cout << "Generating " << ++counter << " cube at positions: " << corner << "\n";
        sponge.add(cube);
    } else {
        for (int z = 0; z < 3; z++) {
            for (int y = 0; y < 3; y++) {
                for (int x = 0; x < 3; x++){
                    if((x != 1 && z != 1) || (x != 1 && y != 1) || (y != 1 && z != 1)){
                        //std::cout << x << " " << y << " " << z << "\n";
                        vec3 subregion_corner = corner + cube_size * vec3(x*pow(3,level-1),y*pow(3,level-1),z*pow(3,level-1));
                        hitable_list subregion = menger_sponge(level-1, cube_size, subregion_corner);
                        sponge.add(subregion);
                    }
                }
            }
        }
    }

    return sponge;
}

int main(int argc, char* args[]) {

    int numThreads = 1;

    for (int i = 1; i < argc;i++){
        std::string arg = args[i];

        if (arg == "--help") {
            std::cout << "To use one thread, add the parameter '-t 0' , to use maximum capacity add '-t 1'" << std::endl;
            return 0;
        }
        else if (arg == "-t") {
            if (i+1 < argc) {
                numThreads = std::stoi(args[i+1]);
                if(numThreads != 0 && numThreads != 1) {
                    std::cout << "Error: Invalid value for '-t' parameter, please use '0' for one thread or '1' for maximum capacity" << std::endl;
                    return 1;
                }
                i++;
            }
            else {
                std::cout << "Error: Number of threads not specified after '-t' parameter." << std::endl;
                return 1;
            }
        }
        else if (arg[0] == '-') {
            std::cout << "Error: Unknown parameters '" << arg << "'" << std::endl;
            return 1;
        }
    }

    int a = 0;
    int level = 1;

    std::cout << "Hello! Welcome to my RayTracer program. Kindly choose a scene:\n1 - 3 Spheres (Material demonstration)\n2 - Cornell box (Lighting demonstration)\n3 - Menger Sponge (Fractal)\nEnter a number 1-3:";
    std::cin >> a;

    if(a > 3 || a < 1){
        a = 3;
    }

    if (a == 3){
        std::cout << "Amazing choice! Now, please specify the level of recursion.\nEnter a number 0-3:";
        std::cin >> level;
    }

    if(level > 3 || level < 0){
        level = 3;
    }

    auto start = std::chrono::high_resolution_clock::now();
    int width;
    int height;
    int samples;
    int fov;
    srand(time(0));

    hitable_list world;

    vec3 lookfrom;
    vec3 lookat;
    vec3 bcg;

    switch (a) {
        default:
        case 1:
            width = 640;
            height = 640;
            samples = 100;
            fov = 45;
            world = sphere_mats();
            bcg = vec3(.7,.8,1);
            lookfrom = vec3(0,0,3);
            lookat = vec3(0,0,-1);
            break;
        case 2:
            width = 640;
            height = 640;
            samples = 100;
            fov = 38;
            world = cornell_box();
            bcg = vec3(0,0,0);
            lookfrom = vec3(278,278,-800);
            lookat = vec3(278,278,0);
            break;
        case 3:
            width = 640;
            height = 640;
            samples = 50;
            vec3 corner(-1,-1,-1);
            vec3 cube_size(3.0/(pow(3, level)),3.0/(pow(3, level)),3.0/(pow(3, level)));
            std::cout << "Generating Menger's sponge... (level: " + std::to_string(level) + ")\n";
            world.add(make_shared<bvh_node>(menger_sponge(level, cube_size, corner), 0, 1));
            std::cout << "Done.\n";
            bcg = vec3(hextorgb("#ff6826"));
            lookfrom = vec3(-4,4,-4);
            lookat = vec3(0, 0.5, 0);
            fov = 45;
            break;
    }

    float dist_to_focus = (lookfrom-lookat).length();
    float aperture = 0;

    std::cout << "------------------------------------\n";
    std::cout << "Width: " << width << "px\nHeight: " << height <<"px\n";
    std::cout << "Samples: " << samples <<"\n";
    std::cout << "FOV: " << fov <<"\370\n";
    std::cout << "------------------------------------\n";

    camera cam(lookfrom, lookat, vec3(0,1,0), fov, float(width)/float(height), aperture, dist_to_focus);

    unsigned int num_threads = 1;
    if(numThreads == 0){
        num_threads = 1;
    } else {
        num_threads = std::thread::hardware_concurrency();
    }
    int rows_per_thread = height / num_threads;

    std::vector<std::thread> threads;
    std::vector<std::vector<vec3>> pixel_data(height, std::vector<vec3>(width));

    std::cout << "Rendering on " + std::to_string(num_threads) + " cores...\n";

    for (int i = 0; i < num_threads; i++) {
        int start_y = height - (i + 1) * rows_per_thread;
        int end_y = start_y + rows_per_thread;
        threads.emplace_back(std::thread([&](int start_y, int end_y) {
            for (int y = start_y; y < end_y; y++) {
                for (int x = 0; x < width; x++) {
                    vec3 col(0, 0, 0);
                    for (int s = 0; s < samples; s++) {
                        float u = float(x + rand01()) / float(width);
                        float v = float(y + rand01()) / float(height);
                        ray r = cam.get_ray(u, v);
                        col += color(r,bcg, world, 50);
                    }
                    col /= float(samples);
                    col = vec3(sqrt(col.r()), sqrt(col.g()), sqrt(col.b()));
                    pixel_data[y][x] = col;
                }
            }
        }, start_y, end_y));
    }

    for (auto& t : threads) {
        if(t.joinable()){
            t.join();
        }
    }

    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "Done, rendering took " << to_ms(end - start).count() << " ms to finish.\n";

    std::string fileName = "raytracer_" + std::to_string(randint(100,999)) + ".ppm";
    if(!pixel_data.empty()){
        output_ppm(width, height, pixel_data, fileName);
    }

    std::cout << "Press any key to end the program...\n";
    std::cin.get();
    std::cin.ignore();
    return 0;
}

