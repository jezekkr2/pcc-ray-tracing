#ifndef RAYTRACER_CAMERA_H
#define RAYTRACER_CAMERA_H

#include "ray.h"

vec3 random_in_unit_disk() {
    vec3 p;
    do{
        p = 2.0*vec3(rand01(), rand01(),0.0) - vec3(1,1,0);
    }while(dot(p,p) >= 1.0);
    return p;
}


class camera {
public:
    camera(vec3 start_point, vec3 target, vec3 up, float fov, float ratio, float aperture, float focus_dist) {


        lens_radius = aperture;
        float theta = fov * M_PI/180;
        float half_height = tan(theta/2);
        float half_width = ratio * half_height;

        origin = start_point;
        w = unit_vector(start_point - target);
        u = unit_vector(cross(up,w));
        v = cross(w,u);

        lower_left_corner = origin - half_width*focus_dist*u - half_height*focus_dist*v - focus_dist*w;

        horizontal = 2*half_width*u*focus_dist;
        vertical = 2*half_height*v*focus_dist;
    }

    ray get_ray(float s, float t) {
        vec3 rd = lens_radius*random_in_unit_disk();
        vec3 offset = u * rd.x() + v * rd.y();
        return ray(origin + offset, lower_left_corner + s*horizontal + t*vertical - origin - offset);
    }

    vec3 lower_left_corner, horizontal, vertical, origin;
    float lens_radius;
    vec3 u,v,w;

};



#endif
