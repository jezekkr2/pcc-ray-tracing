#ifndef HITABLELISTH
#define HITABLELISTH

#include "hitable.h"

#include <memory>
#include <vector>


class hitable_list : public hitable  {
public:
    hitable_list() {}
    hitable_list(shared_ptr<hitable> object) { add(object); }

    void clear() { objects.clear(); }
    void add(shared_ptr<hitable> object) { objects.push_back(object); }
    void add(hitable_list& list) {
        for (int i = 0; i < list.objects.size(); i++) {
            objects.push_back(list.objects[i]);
        }
    }

    virtual bool hit(const ray& r, float t_min, float t_max, hit_record& rec) const;
    virtual bool bounding_box(float time0, float time1, aabb& output_box) const override;

public:
    std::vector<shared_ptr<hitable>> objects;
};


bool hitable_list::hit(const ray& r, float t_min, float t_max, hit_record& rec) const {
    hit_record temp_rec;
    auto hit_anything = false;
    auto closest_so_far = t_max;

    for (const auto& object : objects) {
        if (object->hit(r, t_min, closest_so_far, temp_rec)) {
            hit_anything = true;
            closest_so_far = temp_rec.t;
            rec = temp_rec;
        }
    }

    return hit_anything;
}


bool hitable_list::bounding_box(float time0, float time1, aabb& output_box) const {
    if (objects.empty()) return false;

    aabb temp_box;
    bool first_box = true;

    for (const auto& object : objects) {
        if (!object->bounding_box(time0, time1, temp_box)) return false;
        output_box = first_box ? temp_box : surrounding_box(output_box, temp_box);
        first_box = false;
    }

    return true;
}

#endif