#ifndef RAYTRACER_COMMON_H
#define RAYTRACER_COMMON_H

#include <cmath>
#include <cstdlib>
#include <limits>
#include <memory>
#include <ctime>

// Usings

using std::shared_ptr;
using std::make_shared;
using std::sqrt;

// Constants

const double infinity = std::numeric_limits<double>::infinity();
const double pi = 3.1415926535897932385;

// Utility Functions

inline double degrees_to_radians(double degrees) {
    return degrees * pi / 180.0;
}

inline double clamp(double x, double min, double max) {
    if (x < min) return min;
    if (x > max) return max;
    return x;
}

float rand01(){
    return (float)rand() / RAND_MAX;
}

int randint(int lower, int higher){
    return rand() % higher + lower;
}

float randfloat(float lower, float higher){
    return lower * (lower-higher)*rand01();
}

#endif
