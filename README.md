# PCC Ray Tracing

C++ Raytracer
Tento C++ program implementuje základní raytracer. Je schopen vykreslovat 3 různé scény:

1. Scéna, která testuje různé materiály, jako jsou kovy, plasty a sklo.
2. Klasická scéna Cornell box, což je jednoduchá testovací scéna často používaná v oboru počítačové grafiky.
3. Fraktální scéna Menger sponge, což je složitější a zajímavější scéna k vykreslení.

## Požadavky
- C++17 kompiler
- CMake

## Sestavování
- Naklonujte si repozitář
- Spusťte CMake pro vygenerování souborů pro stavění
- Sestavte program pomocí vašeho vybraného systému pro stavění (např. make, Visual Studio, atd.)

## Použití
Jakmile je program sestaven, můžete ho spustit pomocí následujícího příkazu:

./raytracer -t [0 nebo 1]
- hodnota 0 nebo 1, určuje způsob renderování. 0 znamená vykreslení na jednom vlákně a 1 znamená vykreslení na maximálním počtu dostupných vláken. Pokud není zadán, program se vykreslí na vice vlaknech.


