#ifndef BOX_H
#define BOX_H

#include "rectangle.h"
#include "hitable_list.h"
#include "common.h"
#include "hitable.h"

class boxobject : public hitable  {
public:
    boxobject() {}
    boxobject(const vec3& p0, const vec3& p1, shared_ptr<material> ptr);

    virtual bool hit(const ray& r, float t_min, float t_max, hit_record& rec) const override;

    virtual bool bounding_box(float time0, float time1, aabb& output_box) const override {
        output_box = aabb(box_min, box_max);
        return true;
    }

public:
    vec3 box_min;
    vec3 box_max;
    hitable_list sides;
};


boxobject::boxobject(const vec3& p0, const vec3& p1, shared_ptr<material> ptr) {
    box_min = p0;
    box_max = p1;

    sides.add(make_shared<xy_rect>(p0[0], p1[0], p0[1], p1[1], p1[2], ptr));
    sides.add(make_shared<xy_rect>(p0[0], p1[0], p0[1], p1[1], p0[2], ptr));

    sides.add(make_shared<xz_rect>(p0[0], p1[0], p0[2], p1[2], p1[1], ptr));
    sides.add(make_shared<xz_rect>(p0[0], p1[0], p0[2], p1[2], p0[1], ptr));

    sides.add(make_shared<yz_rect>(p0[1], p1[1], p0[2], p1[2], p1[0], ptr));
    sides.add(make_shared<yz_rect>(p0[1], p1[1], p0[2], p1[2], p0[0], ptr));
}

bool boxobject::hit(const ray& r, float t_min, float t_max, hit_record& rec) const {
    return sides.hit(r, t_min, t_max, rec);
}

#endif
