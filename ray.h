#ifndef RAYH
#define RAYH

#include "vec3.h"

class ray {
public:
    ray() {}
    ray(const vec3& a, const vec3& b) {A=a; B=b; tm=0;}
    ray(const vec3& a, const vec3& b, float time) {A=a; B=b; tm=time;}

    vec3 origin() const {return A;}
    vec3 direction() const {return B;}
    float time() const {return tm;}
    // p(t) = A + t*B
    vec3 point_at_parameter(float t) const {return A + t*B;}

    vec3 A, B;
    float tm;
};

#endif