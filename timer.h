#include "chrono"

#ifndef RAYTRACER_TIMER_H
#define RAYTRACER_TIMER_H

template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

#endif
